<!DOCTYPE html>
<!-- shows multiplication tables -->
<html lang="en">
    <head>
        <title>Exercise 1</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="css/style.css"/>
    </head>
    <body>
        <h1>Exercise 1. Multiplication tables</h1>
        <?php
            require_once 'php/p1-fn.php';
            
            for ($tab=0; $tab<=10; $tab++) {
                print_tab($tab);
            }
        ?>
    </body>
</html>