<?php
    /**
     * performs multiplication of the two provided operands
     * @author cagutiprof
     * @version 2017/October
     * @param $num1 1st operand
     * @param $num2 2nd operand
     * @return result of the calculation
     */
    function multiply(int $num1, int $num2):int {
        return $num1 * $num2;
    }
    
    /**
     * prints a multiplication table
     * @author cagutiprof
     * @version 2017/October
     * @param $tab number of the multiplication table to print
     * @return void
     */
    function print_tab($tab) {
        echo "<table border='1'>";
        echo "<caption>Table $tab</caption>";
        echo "<tr>";
        echo "<th>1st operand</th>";
        echo "<th>2nd operand</th>";
        echo "<th>Result</th>";
        echo "</tr>";

        for ($row=0; $row<=10; $row++) {
            echo "<tr>";
            echo "<td>$tab</td>";
            echo "<td>$row</td>";
            echo "<td>" . multiply($tab, $row) . "</td>";
            echo "</tr>";
        }

        echo "</table>";
    }
