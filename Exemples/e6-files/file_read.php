<?php
    require_once 'php/file-fn.php';

    define('PATH', 'file/');
    define('DELIMITER', ':');
?>
<!DOCTYPE html>
<html lang="en"> 
    <head>
        <title>read file</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="css/style.css"/>
    </head>
    <body>
        <h1>file</h1>

        <?php
            $file_dir=opendir(PATH);
            while ($file_name=readdir($file_dir)) {
                $file_path=PATH . $file_name;
                if (is_file($file_path)) {
                    echo "<p><a href='file_read.php?file=$file_path'>$file_name</a></p>";
                }
            }        

            if (filter_has_var(INPUT_GET, 'file')) {
                $file_path=filter_input(INPUT_GET, 'file');

                $data_read=read_file($file_path, DELIMITER);
                echo "<pre>" . json_encode($data_read, JSON_PRETTY_PRINT) . "</pre>";
            }        
        ?>
    </body>
</html>