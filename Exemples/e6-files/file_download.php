<?php
    if (filter_has_var(INPUT_GET, 'file')) {
        $file_path=filter_input(INPUT_GET, 'file');
        $file_name=basename(filter_input(INPUT_GET, 'file'));

        $file_info=finfo_open(FILEINFO_MIME);
        $file_mime=finfo_file($file_info, $file_path);
        finfo_close($file_info);

        header("Content-Type: $file_mime");
        header("Content-Disposition: attachment; filename=$file_name");
        readfile($file_path);
    }