<?php
    define('PATH', 'file/');
    define('MAX_FILE_SIZE', 1500000); // 1.5 MB
 
    $errors="";
    $file_name="";
    $file_path="";
     
    if (!empty($_FILES['n_file']['name'])) {
        if ($_FILES['n_file']['size']>MAX_FILE_SIZE) {
            $errors="<p>Max file size exceeded</p>";
        }
        else {
            $file_name=basename($_FILES['n_file']['name']);
            // warning permission denied
            $file_path=PATH . $file_name;
            move_uploaded_file($_FILES['n_file']['tmp_name'], $file_path);
            chmod($file_path, 0777);
        }
    }
?>
<!DOCTYPE html>
<html lang="en"> 
    <head>
        <title>upload file</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="css/style.css"/>
        <script>
            function upload() {
                if (confirm('Upload file?')) {
                    document.getElementById('id_form').submit();
                }
                else {
                    alert('Upload cancelled');
                }                
            }
        </script>
    </head>
    <body>
        <!-- debug -->
        <p><?php var_dump($_FILES); ?></p>      
 
        <h1>upload</h1>
 
        <form id="id_form" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
            <fieldset>
                <legend>input</legend>
 
                <h2>file</h2>
                <p>
                    <label for="id_file">Input file:</label>
                    <input type="file" id="id_file" name="n_file"/>
                </p>                
            </fieldset>
            <fieldset>
                <legend>buttons</legend>
 
                <h2>button</h2>
                <p>
                    <input type="button" id="id_upload" name="n_upload" value="upload" onclick="upload();"/>
                </p>
 
                <h2>reset</h2>
                <p>
                    <input type="reset" id="id_reset" name="n_reset" value="reset"/>
                </p>
            </fieldset>
            <fieldset>
                <legend>result</legend>
 
                <?php
                    echo $errors;
 
                    $file_dir=opendir(PATH);
                    while ($file_name=readdir($file_dir)) {
                        $file_path=PATH . $file_name;
                        if (is_file($file_path)) {
                            echo "<p><a href='file_download.php?file=$file_path'>$file_name</a></p>";
                        }
                    }        
                ?>
            </fieldset>
        </form>
    </body>
</html>