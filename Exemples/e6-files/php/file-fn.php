<?php
    /**
     * Read the file content
     * @param type $file_name name of the file to be read
     * @param type $delimiter separator between keys and values
     * @return associative array with the data read from the file
     */
    function read_file(string $file_name, string $delimiter):array {
        $data=array();
        
        if (file_exists($file_name) && is_readable($file_name)) {
            $handle=fopen($file_name, 'r');
            if ($handle) {
                while (!feof($handle)) {
                    $line=fgets($handle);
                    if ($line) {
                        list($key, $value)=explode($delimiter, $line);
                        $data["$key"]=trim($value);
                    }
                }
                fclose($handle);     
            }
        }
        
        return $data;
    }