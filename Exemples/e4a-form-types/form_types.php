<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Forms input types</title>
    </head>
    <body>
        <form action="form_types_result.php" method="post" enctype="multipart/form-data">
            <h1>Forms input types</h1>

            <h2>text</h2>
            <p>
                <label for="form_text">Input text:</label>
                <input type="text" name="form_text" value="Default text" size="20" />
            </p>

            <h2>radio</h2>
            <p>
                <label>Click option:</label>
                <input type="radio" name="form_radio" value="a" checked="checked" />adenine
                <input type="radio" name="form_radio" value="c" />cytosine
                <input type="radio" name="form_radio" value="g" />guanine
                <input type="radio" name="form_radio" value="t" />thymine
            </p>

            <h2>checkbox</h2>
            <p>
                <label>Click multiple options:</label>
                <input type="checkbox" name="form_checkbox[]" value="a" checked="checked" />adenine
                <input type="checkbox" name="form_checkbox[]" value="c" />cytosine
                <input type="checkbox" name="form_checkbox[]" value="g" />guanine
                <input type="checkbox" name="form_checkbox[]" value="t" />thymine
            </p>

            <h2>hidden</h2>
            <p>
                <input type="hidden" name="form_hidden" value="Hidden text" />
            </p>

            <h2>password</h2>
            <p>
                <label>Input password:</label>
                <input type="password" name="form_password" />
            </p>

            <h2>select simple</h2>
            <p>
                <label>Select option:</label>
                <select name="form_select">
                    <option value="-" selected="selected">Default select</option>
                    <option value="a">adenine</option>
                    <option value="c">cytosine</option>
                    <option value="g">guanine</option>
                    <option value="t">thymine</option>
                </select>
            </p>			

            <h2>select multiple</h2>
            <p>
                <label>Select multiple options:</label>
                <select multiple="multiple" name="form_multiselect[]">
                    <option value="a">adenine</option>
                    <option value="c">cytosine</option>
                    <option value="g">guanine</option>
                    <option value="t">thymine</option>
                </select>
            </p>

            <h2>textarea</h2>
            <p>
                <label>Input text:</label>
                <textarea name="form_textarea" cols="50" rows="5">Textarea default text...</textarea>
            </p>

            <h2>button</h2>
            <p>
                <input type="button" name="form_button" value="BUTTON FORM" />
                <!--	<button type="button" name="form_button" value="BUTTON FORM" />  -->
            </p>

            <h2>submit</h2>
            <p>
                <input type="submit" name="form_submit" value="SUBMIT FORM" />
            </p>

            <h2>reset</h2>
            <p>
                <input type="reset" name="form_reset" value="RESET FORM" />
            </p>
        </form>	
    </body>
</html>