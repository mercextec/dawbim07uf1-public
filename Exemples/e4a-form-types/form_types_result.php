<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Forms input types result</title>
    </head>
    <body>
        <h1>Forms input types result</h1>

        <h2>text</h2>
        <?php
        /* $_POST['form_text'] should be changed to filter_input(INPUT_POST,'form_text') */
        echo "<p>", filter_input(INPUT_POST, 'form_text'), "</p>";
        ?>

        <h2>radio</h2>
        <?php
        echo "<p>",filter_input(INPUT_POST,'form_radio'), "</p>";
        ?>

        <h2>checkbox</h2>
        <?php
        $checkboxArray=filter_input(INPUT_POST, 'form_checkbox', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        foreach ($checkboxArray as $checkbox_values) {
            echo "<p>", $checkbox_values, "</p>";
        }
        ?>

        <h2>file</h2>
        <?php
        ?>

        <h2>hidden</h2>
        <?php
        echo "<p>", filter_input(INPUT_POST,'form_hidden'), "</p>";
        ?>

        <h2>password</h2>
        <?php
        echo "<p>",filter_input(INPUT_POST,'form_password'), "</p>";
        ?>

        <h2>select simple</h2>
        <?php
        echo "<p>", filter_input(INPUT_POST,'form_select'), "</p>";
        ?>

        <h2>select multiple</h2>
        <?php
        
        /* isset($_POST['form_multiselect'] should be changed to filter_has_var(INPUT_POST, 'form_multiselect') */
        if (filter_has_var(INPUT_POST, 'form_multiselect')) {
            $multiselectArray=filter_input(INPUT_POST,'form_multiselect', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
            foreach ($multiselectArray as $select_values) {
                echo "<p>", $select_values, "</p>";
            }
        }
        ?>

        <h2>textarea</h2>
        <?php
        echo "<p>", filter_input(INPUT_POST,'form_textarea'), "</p>";
        ?>

        <h2>button</h2>
        <?php
        if (filter_has_var(INPUT_POST,'form_button')) {
            echo "<p>", "'BUTTON FORM' has not clicked", "</p>";
        } else {
            echo "<p>", "'BUTTON FORM' has not been clicked", "</p>";
        }
        ?>

        <h2>submit</h2>
        <?php
        if (filter_has_var(INPUT_POST,'form_submit')) {
            echo "<p>", "'SUBMIT FORM' has been clicked", "</p>";
        }
        ?>

        <p>[<a href="form_types.php">Volver</a>]</p>

        <p>
            <?php
            // for debugging
            var_dump($_POST);
            ?>
        </p>


    </body>
</html>