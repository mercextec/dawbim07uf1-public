<?php
    ini_set('display_errors', 'On');
    error_reporting(E_ALL);
?>
<!DOCTYPE html>
<!--
    comments html
-->
<html lang="en">
    <head>
        <title>index</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="css/style.css"/>
    </head>
    <body>
        <h1>UF1</h1>
        <?php
            // comments php 1
            echo "<p><a href='basic.php'>basic</a></p>";
            /* 
               comments php 2
            */
            echo "<p><a href='form.php'>form</a></p>";
        ?>
    </body>
</html>