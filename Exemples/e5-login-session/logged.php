<?php
  session_start();

  if (!$_SESSION['user_valid']) {
      header("Location: login.php");
  }

  print_r($_SESSION);  
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>logged</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="css/style.css"/>
    </head>
    <body>
        <h1>logged</h1>

        <p>[<a href="logout.php">logout</a>]</p>        
    </body>
</html>
