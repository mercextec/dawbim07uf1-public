<?php
/**
* Example of associative arrays.
*/
echo '<h2>Associative arrays example</h2>';
//define associative array.
$nucleotides = array (
  'a' => 'adenine',
  'c' => 'cytosine',
  'g' => 'guanine',
  't' => 'thymine',
  'u' => 'uracil'
);
echo '<h3>Show nucleotide array with print_r</h3>';
print_r($nucleotides);
echo '<h3>Show nucleotide array with foreach loop</h3>';
foreach ($nucleotides as $key => $value) {
  echo "[$key: $value]";
}
echo '<h3>Get and show array keys</h3>';
$keys = array_keys($nucleotides);
print_r($keys);
echo '<h3>Get and show array values</h3>';
$values = array_values($nucleotides);
print_r($values);
echo '<h3>Exchange keys and values</h3>';
print_r(array_flip($nucleotides));
echo '<h3>Apply funtions to each element of an array</h3>';
print_r(array_map('strtoupper', $values));
echo '<h3>Get the 1st key with a given value</h3>';
print_r(array_search('guanine', $nucleotides));
