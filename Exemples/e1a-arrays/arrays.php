<?php
    /* sum elements of the array */
    echo "<h3>Sum elements of the array</h3>";
    $arr = array(1, 4, 7, 2, 5, 9, 6, 8, 3, 2, 6, 4, 5);
    print_r ($arr);
    $sum = 0;
    foreach ($arr as $num)
        $sum = $sum + $num;
    echo "<p>Sum: $sum</p>";

    /* associative array sorting examples */

    //definition of the hydrogeneted bases.
    $dna_bases = array("t" => "thymine", "a" => "adenine", "g" => "guanine", "c" => "cytosine");

    //sort array by value
    echo "<h3>Sort array by value</h3>";
    sort($dna_bases);
    var_dump($dna_bases);
    echo "<br />";
    print_r($dna_bases);
 
    //sort array by value (reverse)
    echo "<h3>Sort array by value (reverse)</h3>";
    rsort($dna_bases);
    print_r($dna_bases);

    //sort array by key
    echo "<h3>Sort array by key</h3>"; 
    ksort($dna_bases);
    print_r($dna_bases);

    //sort array by key (reverse)
    echo "<h3>Sort array by key (reverse)</h3>"; 
    krsort($dna_bases); 
    print_r($dna_bases);

    /* count number of occurrences of each base in a dna sequence */
    echo "<h3>Count number of occurrences of each base</h3>"; 
    $dna_sequence = "agatggcggcgctgaggggtcttgggggctctaggccggccacctactgg";
    $dna_count = array();
    for ($i = 0; $i < strlen($dna_sequence); $i++) {
        if (array_key_exists($dna_sequence[$i], $dna_count)) {
            $dna_count{$dna_sequence[$i]} ++;  // with arrays, {} and [] are equivalent
        }
        else {
            $dna_count{$dna_sequence[$i]} = 1;
        }
    }
    print_r($dna_count);
?>